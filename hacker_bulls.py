import bulls_and_cows as bc


answers = bc.set_base_answer()
counter = 0
while len(answers) != 1:
    counter += 1
    print("Ход номер {}".format(counter))
    print("Возможных вариантов ответа: {}".format(len(answers)))
    test_answer = bc.get_one_answer(answers)
    print("Назови ответ : {}".format(test_answer))
    bulls = int(input("Быки: "))
    cows = int(input("Коровы: "))
    answers = bc.del_bad_nums(answers, test_answer, bulls, cows)
print("Ответ: {}".format(answers))
