import random


def input_number():
    while True:
        nums = input('Введите 4 неповторяющиеся цифры: ')
        if len(nums) != 4 or not nums.isdigit():
            continue
        nums = list(map(int, nums))
        for num in nums:
            if nums.count(num) > 1:
                break
        else:
            break
    return nums


def get_number():
    num = random.sample(range(0, 10), 4)
    return num


def set_base_answer():
    answer = []
    for i in range(0, 10000):
        temp = str(i).zfill(4)
        is_good_number = ['x' for num in temp if temp.count(num) == 1]
        if is_good_number == ['x', 'x', 'x', 'x']:
            answer.append(list(map(int, temp)))
    return answer


def check_number(nums, true_nums):
    bull, cow = 0, 0
    for i, num in enumerate(nums):
        if num in true_nums:
            if nums[i] == true_nums[i]:
                bull += 1
            else:
                cow += 1
    return bull, cow


def get_one_answer(answers):
    # answer = answers[random.randrange(0, len(answers))]
    answer = random.choice(answers)
    return answer


def del_bad_nums(answers, enemy_try, bull, cow):
    for num in answers[:]:
        temp_bull, temp_cow = check_number(num, enemy_try)
        if temp_bull != bull or temp_cow != cow:
            answers.remove(num)
    return answers


if __name__ == '__main__':
    print('Игра "Быки и коровы"\nЗагадайте число')
    player = input_number()
    enemy = get_number()
    answers = set_base_answer()

    while True:
        # print('Вариантов ответа: ', len(answers))
        print("=" * 10, "Ход игрока", "=" * 10)
        print('Угадайте число противника')
        number = input_number()
        bull, cow = check_number(number, enemy)
        print('Быков: ', bull, ', коров: ', cow)
        if bull == 4:
            print('Вы победили')
            print('Число противника: ', enemy)
            break

        print("=" * 10, "Ход компьютера", "=" * 10)
        enemy_try = get_one_answer(answers)
        print('Противник считает, что у вас загадано число ', enemy_try)
        bull, cow = check_number(enemy_try, player)
        print('Быков: ', bull, ', коров: ', cow)
        if bull == 4:
            print('Вы проиграли')
            print('Число противника: ', enemy)
            break
        else:
            answers = del_bad_nums(answers, enemy_try, bull, cow)
